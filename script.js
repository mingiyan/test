(function (){


    const app = new Vue({
        el: '.container',
        data: {
            todos: [
                {
                    name: 'Some name',
                    description: 'Lorem ipsum',
                    finished: false,
                    priority: 0
                },
                {
                    name: 'mdsafjdiaso fasdlfs',
                    description: 'dnfads fjkdsa fhkjads fjkasd fjks',
                    finished: false,
                    priority: 0
                }


            ],
            name: '',
            description: '',
            priority: '',
            editedIndex: null
        },
        methods: {
            createTodo: function (event) {

                if (this.editedIndex !== null){
                    this.todos[this.editedIndex].name           = this.name;
                    this.todos[this.editedIndex].priority       = Number(this.priority);
                    this.todos[this.editedIndex].description    = this.description;

                    console.log(this.todos[this.editedIndex]);

                }else{
                    this.todos.unshift({

                        name        : this.name,
                        description : this.description,
                        priority    : Number(this.priority),
                        finished    : false
                    });
                    console.log('created');
                }

                this.refresh();


                event.preventDefault();
            },
            readTodo: function (index) {

            },
            updateTodo: function (index) {
                this.editedIndex    = index;
                this.name           = this.todos[index].name;
                this.description    = this.todos[index].description;
                this.priority       = this.todos[index].priority;

                event.preventDefault();
            },
            deleteTodo: function (index) {
                this.todos.splice(index, 1);
            },
            refresh: function () {
                this,name           = "";
                this.description    = "";
                this.priority       = "0";
                this.editedIndex    = null;
            }
        }
    });
    console.log(app);

    window.app = app;
}());